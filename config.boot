interfaces {
    ethernet eth0 {
        address dhcp
        description Outside
        duplex auto
        hw-id 08:00:27:11:91:de
        smp_affinity auto
        speed auto
    }
    ethernet eth1 {
        address 172.16.0.1/24
        description Inside
        duplex auto
        hw-id 08:00:27:a8:2d:fd
        smp_affinity auto
        speed auto
    }
    loopback lo {
    }
}
nat {
    source {
        rule 999 {
            description "NAT inside to outside"
            outbound-interface eth0
            source {
                address 172.16.0.0/24
            }
            translation {
                address masquerade
            }
        }
    }
}
service {
    dhcp-server {
        disabled false
        shared-network-name ETH1_POOL {
            authoritative enable
            subnet 172.16.0.0/24 {
                default-router 172.16.0.1
                dns-server 172.16.0.1
                dns-server 8.8.8.8
                dns-server 8.8.4.4
                lease 86400
                start 172.16.0.100 {
                    stop 172.16.0.199
                }
            }
        }
    }
    dns {
        forwarding {
            cache-size 150
            listen-on eth1
        }
    }
    ssh {
        port 22
    }
}
system {
    config-management {
        commit-revisions 20
    }
    console {
        device ttyS0 {
            speed 9600
        }
    }
    host-name COE-lg
    login {
        user vyos {
            authentication {
                encrypted-password $1$5HsQse2v$VQLh5eeEp4ZzGmCG/PRBA1
            }
            level admin
        }
    }
    name-server 8.8.8.8
    name-server 8.8.4.4
    name-server 209.244.0.3
    name-server 209.244.0.4
    ntp {
        server 0.pool.ntp.org {
        }
        server 1.pool.ntp.org {
        }
        server 2.pool.ntp.org {
        }
    }
    package {
        auto-sync 1
        repository community {
            components main
            distribution helium
            password ""
            url http://packages.vyos.net/vyos
            username ""
        }
    }
    syslog {
        global {
            facility all {
                level notice
            }
            facility protocols {
                level debug
            }
        }
    }
    time-zone UTC
}
vpn {
    ipsec {
        ipsec-interfaces {
            interface eth0
        }
        nat-networks {
            allowed-network 0.0.0.0/0 {
            }
        }
        nat-traversal enable
    }
    l2tp {
        remote-access {
            authentication {
                local-users {
                    username admin {
                        password iwork@COE
                    }
                }
                mode local
            }
            client-ip-pool {
                start 172.16.0.30
                stop 172.16.0.99
            }
            dns-servers {
                server-1 8.8.8.8
                server-2 8.8.4.4
            }
            ipsec-settings {
                authentication {
                    mode pre-shared-secret
                    pre-shared-secret iwork@COE
                }
                ike-lifetime 3600
            }
            mtu 1492
            outside-address 192.168.1.120
        }
    }
}


/* Warning: Do not remove the following line. */
/* === vyatta-config-version: "cluster@1:config-management@1:conntrack-sync@1:conntrack@1:cron@1:dhcp-relay@1:dhcp-server@4:firewall@5:ipsec@4:nat@4:qos@1:quagga@2:system@6:vrrp@1:wanloadbalance@3:webgui@1:webproxy@1:zone-policy@1" === */
/* Release version: VyOS 1.1.3 */
