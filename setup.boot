firewall {
    all-ping enable
    broadcast-ping disable
    config-trap disable
    ipv6-receive-redirects disable
    ipv6-src-route disable
    ip-src-route disable
    log-martians enable
    name WAN_IN {
        default-action drop
        description "packets from Internet to LAN & WLAN"
        enable-default-log
        rule 1 {
            action accept
            description "allow established sessions"
            log disable
            protocol all
            state {
                established enable
                invalid disable
                new disable
                related enable
            }
        }
        rule 2 {
            action drop
            description "drop invalid state"
            log disable
            protocol all
            state {
                established disable
                invalid enable
                new disable
                related disable
            }
        }
    }
    name WAN_LOCAL {
        default-action drop
        description "packets from Internet to the router"
        enable-default-log
        rule 1 {
            action accept
            description "allow established session to the router"
            log disable
            protocol all
            state {
                established enable
                invalid disable
                new disable
                related enable
            }
        }
        rule 2 {
            action drop
            description "drop invalid state"
            log enable
            protocol all
            state {
                established disable
                invalid enable
                new disable
                related disable
            }
        }
    }
    receive-redirects disable
    send-redirects enable
    source-validation disable
    syn-cookies enable
    twa-hazards-protection disable
}
interfaces {
    ethernet eth0 {
        address dhcp
        description Outside
        duplex auto
        hw-id 08:00:27:11:91:de
        smp_affinity auto
        speed auto
    }
    ethernet eth1 {
        address 10.10.10.1/24
        description Inside
        duplex auto
        hw-id 08:00:27:a8:2d:fd
        smp_affinity auto
        speed auto
    }
    loopback lo {
    }
}
nat {
    destination {
        rule 100 {
            description "NAT inbound TCP80,TCP443 to WEB server"
            destination {
                address 192.168.1.13
                port 80,443
            }
            inbound-interface eth0
            protocol tcp
            translation {
                address 10.10.10.100
            }
        }
    }
    source {
        rule 100 {
            description "NAT inside to outside"
            outbound-interface eth0
            source {
                address 10.10.10.0/24
            }
            translation {
                address masquerade
            }
        }
    }
}
service {
    dhcp-server {
        disabled false
        shared-network-name ETH1_POOL {
            authoritative enable
            subnet 10.10.10.0/24 {
                default-router 10.10.10.1
                dns-server 10.10.10.1
                dns-server 8.8.8.8
                dns-server 8.8.4.4
                lease 9999999999
                start 10.10.10.100 {
                    stop 10.10.10.195
                }
            }
        }
    }
    dns {
        forwarding {
            cache-size 150
            listen-on eth1
        }
    }
    ssh {
        port 3574
    }
}
system {
    config-management {
        commit-revisions 20
    }
    console {
        device ttyS0 {
            speed 9600
        }
    }
    domain-name coenterprise.int
    gateway-address 192.168.1.1
    host-name COE-LGA1-CORE
    login {
        user dhughes {
            authentication {
                encrypted-password $6$Lo8MH71/85q.L$3LQN.u6JF.Pk23AHqhiv.jlq.pB5l4K.ClZeTxTZ0JflfIyluL3oflUCDzqicbGvZ3edv0x3prVDnHpjqP26F1
                plaintext-password ""
            }
            full-name "Dustin Hughes"
            level admin
        }
        user vyos {
            authentication {
                encrypted-password !
            }
            level admin
        }
    }
    name-server 8.8.8.8
    name-server 209.244.0.3
	name-server 8.8.4.4
    name-server 209.244.0.4
    ntp {
        server 0.pool.ntp.org {
        }
        server 1.pool.ntp.org {
        }
        server 2.pool.ntp.org {
        }
    }
    package {
        auto-sync 1
        repository community {
            components main
            distribution helium
            password ""
            url http://packages.vyos.net/vyos
            username ""
        }
    }
    syslog {
        global {
            facility all {
                level notice
            }
            facility protocols {
                level debug
            }
        }
    }
    time-zone UTC
}
vpn {
    ipsec {
        ipsec-interfaces {
            interface eth0
        }
        nat-networks {
            allowed-network 0.0.0.0/0 {
            }
        }
        nat-traversal enable
    }
    l2tp {
        remote-access {
            authentication {
                local-users {
                    username admin {
                        password iwork@COE
                    }
					username dhughes {
                        password hpod344odCOE#
                    }
                }
                mode local
            }
            client-ip-pool {
                start 10.10.10.220
                stop 10.10.10.250
            }
            dns-servers {
                server-1 8.8.8.8
                server-2 8.8.4.4
            }
            ipsec-settings {
                authentication {
                    mode pre-shared-secret
                    pre-shared-secret iwork@COE
                }
                ike-lifetime 3600
            }
            mtu 1492
            outside-address 192.168.1.120
        }
    }
}


/* Warning: Do not remove the following line. */
/* === vyatta-config-version: "cluster@1:config-management@1:conntrack-sync@1:conntrack@1:cron@1:dhcp-relay@1:dhcp-server@4:firewall@5:ipsec@4:nat@4:qos@1:quagga@2:system@6:vrrp@1:wanloadbalance@3:webgui@1:webproxy@1:zone-policy@1" === */
/* Release version: VyOS 1.1.3 */
